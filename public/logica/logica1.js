$('.mostrar_senha').click(function () {
  if ($(this).prop("checked") == true) {

    $('.senha').prop('type', 'text')

  } else {

    $('.senha').prop('type', 'password')
  }
})

function verificar_cpf(dados) {

  let cpf = $(dados).val() //Recupera dados no campo

  let valido = false; //variavel para retorno

  cpf = [...cpf] //Converte texto pra array

  let teste = false;

  //verifica se são numeros repetidos
  cpf.forEach(function (valor) {

    if (cpf[0] !== valor) {
      teste = true
    }

  })
  //se não forem numeros reptidos continuar verificacao
  if (teste) {

    let resultado_1 = 0;
    let resultado_2 = 0;

    for (let i = 1; i < 10; i++) {
      //Multiplica os 9 primeiros numeros(separadamente) pela sequencia decrescente de 10 a 2, somando os resultados
      resultado_1 += parseInt(cpf[i - 1], 10) * (11 - i);
    }

    resultado_1 = (resultado_1 * 10) % 11

    if (resultado_2 == 10) {
      resultado_2 = 0
    }


    ///////////////////////////// 2 Teste ////////////////


    if (resultado_1 == parseInt(cpf[9])) {

      for (let i = 1; i <= 10; i++) {
        resultado_2 += parseInt(cpf[i - 1], 10) * (12 - i);
      }

      resultado_2 = (resultado_2 * 10) % 11

      if (resultado_2 == 10) {
        resultado_2 = 0
      }


      if (resultado_2 == parseInt(cpf[10])) {
        valido = true;
      }
    }
  }
  console.log(valido);

  cpf_valido(valido);

}

function cpf_valido(dado) {
  if (dado) {
    $('#cliente_proximo').removeClass('disabled')
    $('.caixa-cliente').attr('action', '/servicos');
    $('#invalido_cpf').html('')
  } else {
    $('#cliente_proximo').addClass('disabled')
    $('.caixa-cliente').attr('action', ' ');
    $('#invalido_cpf').html('CPF Invalido')
  }
}

function obs_valido(dado) {

  dado = $(dado).val()

  if (dado) {
    $('#btn_ok').removeClass('disabled')
  } else {
    $('#btn_ok').addClass('disabled')
  }
}

$('.servico').click(function () {

  let servico = $(this).find('.card-title').html();

  $.ajax({
    url: '/salvar_servico',
    data: {
      'tipo': servico
    },
    cache: false,
    type: 'POST',

    success: function (msg) {
      redireciona(servico);
    }
  })
  //Previne envio involuntario
  return false;

})

function redireciona(servico) {
  switch (servico) {
    case 'Consultar CPF':
      window.open('https://servicos.receita.fazenda.gov.br/servicos/cpf/consultasituacao/consultapublica.asp', '_blank');
      break;
    case 'Consultar CNPJ':
      window.open('http://servicos.receita.fazenda.gov.br/Servicos/cnpjreva/cnpjreva_solicitacao.asp', '_blank');
      break;
  }
}
$('#img_perfil').click(function () {

  if ($('#menu').css('left') == '0px') {
    $('#menu').css('left', '-20em')
  } else {
    $('#menu').css('left', '0em')
  }

})

function copyData(containerid) {
  var range = document.createRange();
  range.selectNode(containerid); //changed here
  window.getSelection().removeAllRanges();
  window.getSelection().addRange(range);
  document.execCommand("copy");
  window.getSelection().removeAllRanges();

  $(containerid).find('i').removeClass('bi bi-clipboard');
  $(containerid).find('i').addClass('bi bi-clipboard-check');

  const timer = setTimeout(function () {

    $(containerid).find('i').removeClass('bi bi-clipboard-check')
    $(containerid).find('i').addClass('bi bi-clipboard');


  }, 3000)

}   
