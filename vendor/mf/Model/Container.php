<?php

namespace mf\Model;

use app\Connection;

class Container{

    static function getModel($model){

        $class = "\\app\\Models\\".ucfirst($model);

        $conn = Connection::getDB();

        return new $class($conn);

    }

}


?>