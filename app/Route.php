<?php

namespace app;

use mf\Init\Bootstrap;

class Route extends Bootstrap{

    protected function initRouts(){
        $routes['home'] = array(
            'route' => '/',
            'controller' => 'indexController',
            'action' => 'index'
        );
        $routes['sobre_nos'] = array(
            'route' => '/sobre',
            'controller' => 'indexController',
            'action' => 'sobre'
        );
        $routes['atendimento'] = array(
            'route' => '/atendimento',
            'controller' => 'AppController',
            'action' => 'atendimento'
        );
        $routes['servicos'] = array(
            'route' => '/servicos',
            'controller' => 'AppController',
            'action' => 'servicos'
        );
        $routes['autenticar'] = array(
            'route' => '/autenticar',
            'controller' => 'AuthController',
            'action' => 'autenticar'
        );
        $routes['sair'] = array(
            'route' => '/sair',
            'controller' => 'AuthController',
            'action' => 'sair'
        );
        $routes['dados_cliente'] = array(
            'route' => '/dados_cliente',
            'controller' => 'AppController',
            'action' => 'dados_cliente'
        );
        $routes['salvar_servico'] = array(
            'route' => '/salvar_servico',
            'controller' => 'AppController',
            'action' => 'salvar_servico'
        );
        $routes['observacao'] = array(
            'route' => '/observacao',
            'controller' => 'AppController',
            'action' => 'observacao'
        );
        $routes['fim_obervacao'] = array(
            'route' => '/fim_obervacao',
            'controller' => 'AppController',
            'action' => 'fim_obervacao'
        );
        $routes['gerenciar_usuarios'] = array(
            'route' => '/gerenciar_usuarios',
            'controller' => 'AppController',
            'action' => 'gerenciar_usuarios'
        );
        $routes['relatorios'] = array(
            'route' => '/relatorios',
            'controller' => 'AppController',
            'action' => 'relatorios'
        );
        $routes['quant_servico'] = array(
            'route' => '/quant_servico',
            'controller' => 'AppController',
            'action' => 'quant_servico'
        );

        $this->setRoutes($routes);

    }
}
?>