<?php

namespace app;

use PDOException;

class Connection{
    
    static function getDB(){
        //Cria conexão com o banco
        try{
            $conn = new \PDO(
                "mysql:host=localhost;
                dbname=bd_naf;
                charset=utf8",
                "root",//Usuario
                ""//senha
            );

            return $conn;
        }catch(\PDOException $e){
            //Tratar erro 
        }
    }
}

?>