<?php

namespace app\Models;

use mf\Model\Model;

class Atendimento extends Model
{

    private $id_atendimento,
        $fk_usuario,
        $nome_cliente,
        $cpf_cliente,
        $data_atd,
        $hora_inicio,
        $hora_fim,
        $observacao;

    function __get($atributo)
    {
        return $this->$atributo;
    }

    function __set($atributo, $valor)
    {
        $this->$atributo = $valor;
    }

    function criar_atendimento()
    {
        $query = 'insert into tb_atendimento (fk_usuario)
        values(:fk_usuario)';

        $stmt = $this->db->prepare($query);

        $stmt->bindValue(':fk_usuario', $this->__get('fk_usuario'));

        $stmt->execute();
    }
    function salvar_cliente()
    {

        $query = "  update
                        tb_atendimento
                    SET
                        nome_cliente = :nome_cliente,
                        cpf_cliente = :cpf_cliente

                    WHERE
                        id_atendimento = :id_atendimento";

        $stmt = $this->db->prepare($query);

        $stmt->bindValue(':id_atendimento', $this->__get('id_atendimento'));
        $stmt->bindValue(':nome_cliente', $this->__get('nome_cliente'));
        $stmt->bindValue(':cpf_cliente', $this->__get('cpf_cliente'));

        $stmt->execute();
        
    }
    function deleta_atd_nulo()
    {
        //tem que deletar os serviços primeiro
        $query = "delete  FROM
                    tb_atendimento
                WHERE
                    fk_usuario = :fk_usuario AND (nome_cliente IS null OR cpf_cliente IS null)";

        $stmt = $this->db->prepare($query);

        $stmt->bindValue(':fk_usuario', $this->__get('fk_usuario'));

        $stmt->execute();
    }
    function encerrar()
    {
        $query = " update
                        tb_atendimento
                    SET
                        h_fim = :h_fim
                    WHERE
                        id_atendimento = :id_atendimento";

        $stmt = $this->db->prepare($query);

        $stmt->bindValue(':id_atendimento', $this->__get('id_atendimento'));
        $stmt->bindValue(':h_fim', $this->__get('hora_fim'));

        $stmt->execute();
    }

    function salvar_obs(){

        $query = " update
                        tb_atendimento
                    SET
                        observacao = :observacao
                    WHERE
                        id_atendimento = :id_atendimento";

        $stmt = $this->db->prepare($query);

        $stmt->bindValue(':id_atendimento', $this->__get('id_atendimento'));
        $stmt->bindValue(':observacao', $this->__get('observacao'));

        $stmt->execute();
    }
    function recupera_id()
    {
        $query = "select
                    id_atendimento
                FROM
                    `tb_atendimento`
                WHERE
                    data_atd =(
                    SELECT
                        MAX(data_atd)
                    FROM
                        tb_atendimento
                ) AND fk_usuario = :fk_usuario
                ORDER BY
                    h_inicio
                DESC
                LIMIT 1";

        $stmt = $this->db->prepare($query);

        $stmt->bindValue(':fk_usuario', $this->__get('fk_usuario'));

        $stmt->execute();

        $stmt = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $stmt[0]['id_atendimento'];
    }
}
