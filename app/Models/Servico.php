<?php

namespace app\Models;

use mf\Model\Model;

class Servico extends Model
{

    private $id_servico,
        $fk_atendimento,
        $tipo;

    function __get($atributo)
    {
        return $this->$atributo;
    }

    function __set($atributo, $valor)
    {
        $this->$atributo = $valor;
    }

    function salvar()
    {
        //Um serviço só pode ser adicionado 1 vez em 1 atendimento
        if (!count($this->verifica_se_exixte())) {
            $query = 'insert into tb_servico (fk_atendimento, tipo)
                        values(:fk_atendimento,:tipo)';

            $stmt = $this->db->prepare($query);

            $stmt->bindValue(':fk_atendimento', $this->__get('fk_atendimento'));
            $stmt->bindValue(':tipo', $this->__get('tipo'));

            $stmt->execute();
        }
    }
    function verifica_se_exixte()
    {

        $query = 'select * from tb_servico 
        where fk_atendimento = :fk_atendimento and tipo = :tipo';

        $stmt = $this->db->prepare($query);

        $stmt->bindValue(':fk_atendimento', $this->__get('fk_atendimento'));
        $stmt->bindValue(':tipo', $this->__get('tipo'));

        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    function quantidade()
    {
        $query = "select count(*) FROM tb_servico WHERE tipo = :tipo";

        $stmt = $this->db->prepare($query);
        $stmt->bindValue(':tipo', $this->__get('tipo'));
        $stmt->execute();
        
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}
