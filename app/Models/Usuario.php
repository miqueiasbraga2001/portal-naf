<?php

namespace app\Models;

use mf\Model\Model;

class Usuario extends Model
{

    private $id,
            $email,
            $nome,
            $senha,
            $quant_av,
            $soma_av,
            $nivel;

    function __get($atributo){
        return $this->$atributo;
    }

    function __set($atributo, $valor){
        $this->$atributo = $valor;
    }


    function salvar()
    {
        $query = 'insert into tb_usuarios (email,nome,senha,cep,uf,cidade,bairro,rua)
                  values(:email,:nome,:senha,:cep,:uf,:cidade,:bairro,:rua)';
        $stmt = $this->db->prepare($query);

        $stmt->bindValue(':senha', md5($this->__get('senha')));
        $stmt->bindValue(':email', $this->__get('email'));
        $stmt->bindValue(':nome', $this->__get('nome'));
        $stmt->bindValue(':cep', $this->__get('cep'));
        $stmt->bindValue(':uf', $this->__get('uf'));
        $stmt->bindValue(':cidade', $this->__get('cidade'));
        $stmt->bindValue(':bairro', $this->__get('bairro'));
        $stmt->bindValue(':rua', $this->__get('rua'));

        $stmt->execute();

        return $this;
    }

    function validar_cadastro()
    {
        $valido = true;


        if (strlen($this->__get('nome')) < 3) {
            $valido = 'nome<3';
        } else if (strlen($this->__get('senha')) < 8) {
            $valido = 'senha<8';
        } else if ($this->getUsuario('email')) {
            $valido = 'email_existente';
        } else if ($this->getUsuario('nome')) {
            $valido = 'nome_existente';
        }

        ////////////////////////////verifica Vazio
        $dados = ['nome', 'email', 'senha', 'cep', 'uf', 'cidade', 'bairro', 'rua'];

        foreach ($dados as $key => $value) {
            if ($this->__get($value) == '') {
                $valido = 'vazio';
            }
        }
        return $valido;
    }
    //Recupera ou não um usuario baseado em um parametro WHERE 
    //Retorna True se exixtir e Flase se nao exixtir
    function getUsuario($atributo, $cript = false)
    {

        if ($cript) {

            $valor = strrev($this->__get($atributo));

            $query = "select $atributo from tb_usuarios
            where MD5($atributo) = :valor";
        } else {

            $valor = $this->__get($atributo);

            $query = "select $atributo from tb_usuarios
            where
            $atributo = :valor";
        }

        $stmt = $this->db->prepare($query);

        $stmt->bindValue(":valor", $valor);

        $stmt->execute();

        //Verifica array associativo
        //Para ver quantos elentos exixtem
        if (count($stmt->fetchAll(\PDO::FETCH_ASSOC)) > 0) {

            return true; //Já Exixte
        } else {
            return false; //Não Exixte
        }
    }

    function autenticar(){
    

        $query = 'select id_usuario,nome,email,quant_av,soma_av,nivel,imagem
                    from tb_usuario
                    where
                    email = :email   and
                    senha = :senha';

        $stmt = $this->db->prepare($query);
        $stmt->bindValue(":email", $this->__get('email'));
        $stmt->bindValue(":senha", $this->__get('senha'));
        $stmt->execute();

        $usuario = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $status = 'invalido';

        //Se Maior que 0 então o Usuario existe
        if (count($usuario)) {

            //Seleciona o primeiro indice do array
            //para ficar mais facil de manipular
            $usuario = $usuario[0];

            $status = 'valido';

            //Guardo Valores no Objeto $usuario
            //Ele ja possui email e senha        
            $this->__set('id', $usuario['id_usuario']);
            $this->__set('nome', $usuario['nome']);
            $this->__set('quant_av', $usuario['quant_av']);
            $this->__set('soma_av', $usuario['soma_av']);
            $this->__set('nivel', $usuario['nivel']);
            $this->__set('imagem', $usuario['imagem']);
            
        }
        return $status;
    }
}
