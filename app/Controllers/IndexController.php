<?php

namespace app\Controllers;
//Recurssos do Miniframework
use mf\Model\Container;
use mf\Controller\Action;

//Models
use app\Models\Produto;
use app\Models\Info;


class IndexController extends Action{
   
    function index(){

        //Se exixtir recebe o valor
        $this->view->status = isset($_GET['status']) ? $_GET['status'] : '';

        $this->render('index', 'layout1');
    }
    function sobre(){

        $this->render('sobre','layout1');
    }

}

?>