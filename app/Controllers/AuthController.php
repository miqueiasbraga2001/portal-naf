<?php

namespace app\Controllers;

//Recurssos do Miniframework
use mf\Model\Container;
use mf\Controller\Action;

class AuthController extends Action
{

    function autenticar()
    {

        $usuario = Container::getModel('Usuario');

        $usuario->__set('email', $_POST['email']);
        $usuario->__set('senha', $_POST['senha']);

        $status = $usuario->autenticar();

        if ($status == 'valido') {

            session_start();

            $_SESSION['id'] = $usuario->__get('id');
            $_SESSION['nome'] = $usuario->__get('nome');
            $_SESSION['email'] = $usuario->__get('email');
            $_SESSION['quant_av'] = $usuario->__get('quant_av');
            $_SESSION['soma_av'] = $usuario->__get('soma_av');
            $_SESSION['nivel'] = $usuario->__get('nivel');
            $_SESSION['imagem'] = $usuario->__get('imagem');


            header('Location:/atendimento');
            
        } else if ($status == 'invalido') {
            header("Location:/?status=invalido");
        }
    }

    function sair()
    {

        session_start();
        session_destroy();
        header('Location:/');
    }
}
