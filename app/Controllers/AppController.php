<?php

namespace app\Controllers;

//Recurssos do Miniframework
use mf\Model\Container;
use mf\Controller\Action;

class AppController extends Action
{

    function atendimento()
    {
      
        $this->verifica_session();
        
        $this->render('atendimento', 'layout1');
    }
    function dados_cliente()
    {

        $this->verifica_session();

        $this->pagina_anterior('atendimento','fim_obervacao');

        $atendimento = Container::getModel('Atendimento');

        $atendimento->__set('fk_usuario', $_SESSION['id']);

        $atendimento->deleta_atd_nulo();

        $atendimento->criar_atendimento();

        $this->render('dados_cliente', 'layout1');
    }
    function servicos()
    {

        $this->verifica_session();

        $this->pagina_anterior('dados_cliente');

        $atendimento = Container::getModel('Atendimento');

        $atendimento->__set('fk_usuario', $_SESSION['id']);
        $atendimento->__set('nome_cliente', $_POST['nome']);
        $atendimento->__set('cpf_cliente', $_POST['cpf']);

        $atendimento->__set('id_atendimento', $atendimento->recupera_id());

        $atendimento->salvar_cliente();
        $this->view->nome_cliente = $_POST['nome'];
        $this->view->cpf_cliente = $_POST['cpf'];

        $this->render('servicos', 'layout1');
    }
    function salvar_servico()
    {
        $this->verifica_session();
        $this->pagina_anterior('servicos');

        $atendimento = Container::getModel('Atendimento');
        $atendimento->__set('fk_usuario', $_SESSION['id']);

        $servico = Container::getModel('Servico');

        $servico->__set('fk_atendimento', $atendimento->recupera_id());
        $servico->__set('tipo', $_POST['tipo']);

        $servico->salvar();
    }
    function observacao()
    {

        $this->verifica_session();
        $this->pagina_anterior('servicos');

        $Object = new \DateTime();
        $Object->setTimezone(new \DateTimeZone('America/Sao_Paulo'));
        $DateAndTime = $Object->format("H:i:s");
        
        $atendimento = Container::getModel('Atendimento');

        $atendimento->__set('fk_usuario', $_SESSION['id']);
        $atendimento->__set('hora_fim', $DateAndTime);
        $atendimento->__set('id_atendimento', $atendimento->recupera_id());

        $atendimento->encerrar();

        $this->render('observacao', 'layout1');
    }
    function fim_obervacao()
    {
        $this->verifica_session();
        $this->pagina_anterior('observacao');

        $atendimento = Container::getModel('Atendimento');

        $atendimento->__set('fk_usuario', $_SESSION['id']);
        if(isset( $_POST['observacao'])){
            $atendimento->__set('observacao', $_POST['observacao']);
            $atendimento->__set('id_atendimento', $atendimento->recupera_id());
    
            $atendimento->salvar_obs();
    
        }
        
        $this->render('atendimento', 'layout1');
    }
    function gerenciar_usuarios()
    {

        $this->verifica_session();

        if ($_SESSION['nivel'] < 3) {
            header('Location:/atendimento');
        } else {
            $this->view->nivel = $_SESSION['nivel'];
            $this->render('gerenciar_usuarios', 'layout1');
        }
    }
    function relatorios()
    {
        $this->verifica_session();

        if ($_SESSION['nivel'] < 2) {
            header('Location:/atendimento');
        } else {
            $this->view->quant_servico['Consultar CPF'] = $this->quant_servico('Consultar CPF');
            $this->view->quant_servico['Consultar CNPJ'] = $this->quant_servico('Consultar CNPJ');
            $this->view->nivel = $_SESSION['nivel'];

            $this->render('relatorios', 'layout1');
        }
    }

    function verifica_session()
    {

        session_start();

        //Se estiverem Vazios  ou Não existirem      
        if (
            empty($_SESSION['id']) || empty($_SESSION['nome']) || empty($_SESSION['email'])
            || !isset($_SESSION['id']) || !isset($_SESSION['id']) || !isset($_SESSION['id'])
        ) {

            header('Location:/?status=invalido');
        }
    }
    function pagina_anterior($pagina, $pagina2=0)
    {
        if(isset($_SERVER['HTTP_REFERER'])){

            $url = $_SERVER['HTTP_REFERER'];
            $url = str_replace("http://localhost:8080/", "", $url);

            if ($url != $pagina && $url != $pagina2) {
                
                header('Location:/atendimento');     
            }
        }
        else{
            header('Location:/atendimento');     
        }
        
    }
    function quant_servico($tipo){

        $servico = Container::getModel('Servico');

        $servico->__set('tipo', $tipo);

        $quant = $servico->quantidade();
        $quant = $quant[0]['count(*)'];

        return $quant;
    }
}
