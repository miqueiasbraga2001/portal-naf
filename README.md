# Portal NAF
Portal para recuperação de dados de atendimento do sistema NAF

[O que é o NAF?](https://www.gov.br/receitafederal/pt-br/assuntos/educacao-fiscal/educacao-fiscal/naf#:~:text=O%20N%C3%BAcleo%20de%20Apoio%20Cont%C3%A1bil,por%C3%A9m,%20um%20escrit%C3%B3rio%20de%20contabilidade.)

<img src="https://www.gov.br/receitafederal/pt-br/assuntos/noticias/2019/maio/naf-oferece-orientacao-para-a-declaracao-anual-do-mei/nova-logo.png" width="300px"/>

## Objetivo do Portal
- Plataformas especificas para cada instituição de que realize o serviço
- Obtenção de dados dos atendimentos
- Monitoramento especifico de cada atendente
- Visualizar os dados obitidos através de gráficos
